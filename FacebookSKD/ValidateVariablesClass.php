<?php

/**
 * Class ValidateVariablesClass
 */
class ValidateVariablesClass
{
    /**
     * ValidateVariablesClass constructor.
     */
    public function __construct()
    {
        // load env file
        $dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
        $dotenv->load();
    }

    /**
     * Simple env variable validation
     *
     * @param array $requiredVariables
     * @return bool|null
     */
    public function valid(array $requiredVariables): ?bool
    {
        foreach ($requiredVariables as $requiredVariable) {

            // check if value is defined
            if(!isset($_ENV[$requiredVariable])) {
                echo "ERROR: env variable " . $requiredVariable . ' is required!';
                return false;
            }

            // check if value is empty
            if($_ENV[$requiredVariable] === '') {
                echo "ERROR: env variable " . $requiredVariable . ' can\'t be empty!';
                return false;
            }
        }

        return true;
    }
}