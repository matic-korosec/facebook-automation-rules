<?php
// LOAD CLASSES AND COMPOSER
require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/DeleteAllRulesClass.php';
require __DIR__ . '/ValidateVariablesClass.php';

// load env file
$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

/**
 * constant variables were moved to .env file loading
 * look at .env.example in this folder for more information
 * if you want to create new .env file, just run command in this folder: 'cp .env.example .env' and change variables as needed
 */

// validate that all env variables are defined
$requiredVariables = [
    'ACCESS_TOKEN',
    'APP_SECRET',
    'APP_ID',
    'DELETE_ALL_RULES_AD_ACCOUNT_ID',
];

/*
 * BEGIN DELETING RULES
 * !Script can only delete rules that were created by this user!
 */

echo  PHP_EOL . "WARNING: DELETE RULES COMMAND STARTED" . PHP_EOL;
echo "YOU HAVE 7 SECONDS TO STOP THIS COMMAND FROM EXECUTING (ctrl + c)" . PHP_EOL . PHP_EOL;

for ($st = 6; $st >= 0; $st--) {
    echo $st . PHP_EOL;
    sleep(1);
}
echo 'STARTED' . PHP_EOL. PHP_EOL;


$validation = (new ValidateVariablesClass())->valid([
    'ACCESS_TOKEN',
    'APP_SECRET',
    'APP_ID',
    'DELETE_ALL_RULES_AD_ACCOUNT_ID',
]);

if($validation === false) {
    return;
}

echo PHP_EOL . PHP_EOL . "**************** DELETE RULES STARTED ********************" . PHP_EOL . PHP_EOL;

// delete all rules
(new DeleteAllRulesClass())->execute();

echo PHP_EOL . PHP_EOL . "************  DELETE RULES COMPLETED ****************" . PHP_EOL . PHP_EOL;