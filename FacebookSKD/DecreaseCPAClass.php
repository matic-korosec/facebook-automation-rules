<?php

use FacebookAds\Object\AdAccount;
use FacebookAds\Api;
use FacebookAds\Logger\CurlLogger;

/**
 * Class DecreaseCPAClass
 */
class DecreaseCPAClass
{
    // How many times loop should execute
    public int $LOOP_LIMIT = 120;
    // starting CPA score
    public float $BASE_CPA_SCORE = 4.0;
    // CPA increment
    public float $MAX_CPA_INCREMENT = 0.1;

    /**
     * @var float
     */
    protected float $currentCPA;

    /**
     * @var string
     */
    protected string $campaignLongName;

    /**
     * @var string
     */
    protected string $campaignShortName;

    /**
     * @var float
     */
    protected float $costPerResult;

    /**
     * DecreaseCPAClass constructor.
     */
    public function __construct()
    {
        $dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
        $dotenv->load();

        $this->LOOP_LIMIT = $_ENV['DECREASE_CPA_LOOP_LIMIT'];
        $this->BASE_CPA_SCORE = $_ENV['DECREASE_CPA_BASE_SCORE'];
        $this->MAX_CPA_INCREMENT = $_ENV['DECREASE_CPA_INCREMENT'];
        $this->currentCPA = $this->BASE_CPA_SCORE;
    }

    /**
     * Create rules based on constants
     */
    public function execute(): void
    {
        for ($st = 0; $st < $this->LOOP_LIMIT; $st++) {
            // set campaign name
            $this->setCampaignNames();
            // calculate CPR (cost per result)
            $this->calculateCPA();

            // make request
            $this->createRuleRequest();

            // increment maxCpa
            $this->currentCPA += $this->MAX_CPA_INCREMENT;
        }
    }

    /**
     * Set campaign names
     */
    protected function setCampaignNames(): void
    {
        $this->campaignShortName = 'MAX : ' . number_format($this->currentCPA, 1);
        // Increase CPA>3. campaigns budget by 15%
        $this->campaignLongName = 'Decrease CPA>' . number_format($this->currentCPA, 1) . ' campaigns budget by 15%';
    }

    /**
     * Calculate decrease CPA cost per result. Should be in cents!
     *
     * @return void
     */
    protected function calculateCPA(): void
    {
        $this->costPerResult = ($this->currentCPA + ($this->currentCPA * 0.08)) * 100;
    }

    /**
     * Execute request for decreased CPA
     */
    protected function createRuleRequest(): void
    {
        try {
            $api = Api::init($_ENV['APP_ID'], $_ENV['APP_SECRET'], $_ENV['ACCESS_TOKEN']);
            $api->setLogger(new CurlLogger());

            $fields = [];
            $params = [
                'name' => $this->campaignLongName,
                'schedule_spec' => [
                    // we run this every day, every 30 min
                    'schedule_type' => 'SEMI_HOURLY',
                ],
                'evaluation_spec' => [
                    'evaluation_type' => 'SCHEDULE',
                    'filters' => [
                        [
                            "field" => 'campaign.name',
                            "value" => $this->campaignShortName,
                            "operator" => 'CONTAIN'
                        ],
                        [
                            'field' => 'cost_per',
                            'value' => $this->costPerResult,
                            'operator' => 'GREATER_THAN'
                        ],
                        [
                            'field' => 'spent',
                            'value' => '1000',
                            'operator' => 'GREATER_THAN'
                        ],
                        [
                            'field' => 'campaign.budget_reset_period',
                            'value' => [
                                'DAY'
                            ],
                            'operator' => 'IN'
                        ],
                        [
                            'field' => 'entity_type',
                            'value' => 'CAMPAIGN',
                            'operator' => 'EQUAL'
                        ],
                        [
                            'field' => 'time_preset',
                            'value' => 'TODAY',
                            'operator' => 'EQUAL'
                        ],
                        [
                            'field' => 'attribution_window',
                            'value' => 'ACCOUNT_DEFAULT',
                            'operator' => 'EQUAL'
                        ]
                    ]
                ],
                'execution_spec' =>
                    [
                        'execution_type' => 'CHANGE_CAMPAIGN_BUDGET',
                        'execution_options' => [
                            /*
                             * this would allow subscribing specific user
                            [
                                'field' => 'user_ids',
                                'value' =>
                                    [
                                        '10221477466158739'
                                    ],
                                'operator' => 'EQUAL'
                            ],*/
                            [
                                'field' => 'action_frequency',
                                'value' => '60',
                                'operator' => 'EQUAL'
                            ],
                            [
                                'field' => 'alert_preferences',
                                'value' => [
                                    'instant' => [
                                        'trigger' => 'CHANGE',
                                    ]
                                ],
                                'operator' => 'EQUAL'
                            ],
                            [
                                'field' => 'change_spec',
                                'value' => [
                                    'amount' => '-15',
                                    'limit' => '1000',
                                    'unit' => 'PERCENTAGE',
                                    'target_field' => null,
                                ],
                                'operator' => 'EQUAL'
                            ],
                        ]
                    ],
            ];

            (new AdAccount($_ENV['AD_ACCOUNT_ID']))->createAdRulesLibrary(
                $fields,
                $params
            );

            echo 'Rules creation successful: ' . $this->campaignLongName;

        } catch (Throwable $e) {
            echo 'ERROR for campaign: ' . $this->campaignLongName;
            echo $e->getMessage();
        }
    }
}