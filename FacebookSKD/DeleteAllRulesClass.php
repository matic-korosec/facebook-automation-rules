<?php

use FacebookAds\Object\AdAccount;
use FacebookAds\Api;
use FacebookAds\Logger\CurlLogger;

/**
 * Class DeleteAllRulesClass
 */
class DeleteAllRulesClass
{
    /**
     * @var array|null
     */
    protected array $rules;

    /**
     * @var Api
     */
    protected Api $client;

    /**
     * DecreaseCPAClass constructor.
     */
    public function __construct()
    {
        $dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
        $dotenv->load();
    }

    /**
     * Create rules based on constants
     */
    public function execute(): void
    {
        try {
            $this->getClient();
            // get all rules
            $this->getRulesForThisUser();
            // delete rules
            $this->deleteRules();
        } catch (Throwable $e) {
            echo PHP_EOL . PHP_EOL . "ERROR accrued" . PHP_EOL . PHP_EOL;
            echo $e->getMessage();
            return;
        }
    }

    protected function deleteRules(): void
    {
        echo PHP_EOL . 'Deleted rules:' . PHP_EOL .PHP_EOL;
        foreach ($this->rules as $rule) {
            // this method is marked as deprecated
            $rule->deleteSelf();

            $ruleId = $this->getDeletedRuleId($rule, 'data', 'id');
            echo $ruleId. PHP_EOL;
        }
    }

    /**
     * Get all rules for this ad account
     */
    protected function getRulesForThisUser(): void
    {
        $fields = [];
        $params = [];

        $rules = (new AdAccount($_ENV['DELETE_ALL_RULES_AD_ACCOUNT_ID']))->getAdRulesLibrary(
            $fields,
            $params
        );

        $this->rules = $rules->getArrayCopy();
    }

    /**
     * Get rule id
     * This property is protected, so use reflection
     *
     * @param $obj
     * @param $prop
     * @param $arrayKey
     * @return mixed|null
     */
    protected function getDeletedRuleId($obj, $prop, $arrayKey)
    {
        try {
            $reflection = new ReflectionClass($obj);
            $property = $reflection->getProperty($prop);
            $property->setAccessible(true);
            $val = $property->getValue($obj);
            return $val[$arrayKey];
        } catch (ReflectionException $e) {
            return null;
        }
    }

    /**
     * Get client instance
     *
     * @return Api
     */
    protected function getClient(): Api
    {
        if (!isset($this->client)) {
            $this->client = Api::init($_ENV['APP_ID'], $_ENV['APP_SECRET'], $_ENV['ACCESS_TOKEN']);
            $this->client->setLogger(new CurlLogger());
        }

        return $this->client;
    }
}