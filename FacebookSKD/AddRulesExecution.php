<?php
// LOAD CLASSES AND COMPOSER
require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/DecreaseCPAClass.php';
require __DIR__ . '/IncreaseCPAClass.php';
require __DIR__ . '/ValidateVariablesClass.php';

// load env file
$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

/**
 * constant variables were moved to .env file loading
 * look at .env.example in this folder for more information
 * if you want to create new .env file, just run command in this folder: 'cp .env.example .env' and change variables as needed
 */

/*
 * BEGIN ADDING RULES
 */

$queryOverview = new LucidFrame\Console\ConsoleTable();
$queryOverview
    ->addHeader('Type')
    ->addHeader('Starting score')
    ->addHeader('Increase increment')
    ->addHeader('Loop limit')
    ->addRow()
    ->addColumn('Decrease CPA')
    ->addColumn($_ENV['DECREASE_CPA_BASE_SCORE'])
    ->addColumn($_ENV['DECREASE_CPA_INCREMENT'])
    ->addColumn($_ENV['DECREASE_CPA_LOOP_LIMIT'])
    ->addRow()
    ->addColumn('Increase CPA')
    ->addColumn($_ENV['INCREASE_CPA_BASE_SCORE'])
    ->addColumn($_ENV['INCREASE_CPA_INCREMENT'])
    ->addColumn($_ENV['INCREASE_CPA_LOOP_LIMIT'])
    ->addRow()
    ->display();


echo PHP_EOL . PHP_EOL . "**************** DECREASE CPA STARTED ********************" . PHP_EOL . PHP_EOL;

// validate that all env variables are defined
$validation = (new ValidateVariablesClass())->valid([
    'ACCESS_TOKEN',
    'APP_SECRET',
    'APP_ID',
    'AD_ACCOUNT_ID',
    'INCREASE_CPA_LOOP_LIMIT',
    'INCREASE_CPA_BASE_SCORE',
    'INCREASE_CPA_INCREMENT',
]);

if($validation === false) {
    return;
}

// decrease CPA rules
(new DecreaseCPAClass())->execute();

echo PHP_EOL . PHP_EOL . "************ BEGIN CREATION  DECREASE CPA ****************" . PHP_EOL . PHP_EOL;

// validate that all env variables are defined
$validation = (new ValidateVariablesClass())->valid([
    'ACCESS_TOKEN',
    'APP_SECRET',
    'APP_ID',
    'AD_ACCOUNT_ID',
    'DECREASE_CPA_LOOP_LIMIT',
    'DECREASE_CPA_BASE_SCORE',
    'DECREASE_CPA_INCREMENT',
]);


if($validation === false) {
    return;
}

// increase CPA rules
(new IncreaseCPAClass())->execute();

echo PHP_EOL . PHP_EOL . "****************** STOPPING SERVICE **********************";